open Chrome
open Common.Types

module Lib(S: S) = struct
  include Make(S)

  let send_req ?callback ?id req_input =
    let info = Utils.Runtime.mk_connection_info "client" in
    let req_id = match id with
      | None -> Int32.to_int @@ Random.int32 Int32.max_int
      | Some i -> i in
    try
      let req = request_aux_to_jsoo S.request_jsoo_conv
          {req_id; req_input; req_src=`client} in
      let port = Runtime.connect ~info () in
      match callback with
      | None ->
        port##postMessage req ;
        port##disconnect
      | Some cb ->
        Utils.Browser.addListener1 port##.onMessage (fun res ->
            try
              let res = response_aux_of_jsoo response_jsoo_conv res in
              match res.res_src with
              | `background when res.res_id = req_id ->
                cb res.res_output; port##disconnect
              | _ -> ()
            with exn ->
              cb (Error (`generic ("wrong response", Printexc.to_string exn))));
        port##postMessage req
    with exn ->
    match callback with
    | None -> raise exn
    | Some cb -> cb (Error (`generic ("wrong request", Printexc.to_string exn)))

  let () = Random.self_init ()
end
