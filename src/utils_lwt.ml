open Chrome_lwt
open Common

module Make(S : Types.S) = struct
  let popup ?(base="notification.html") ?(width=300) ?(height=600) ?id ?(callback : (Windows.window Ezjs_min.t -> unit) option) request =
    let id = Option.fold ~none:"" ~some:(fun i -> Format.sprintf "&id=%d" i) id in
    let req = encode ((fst S.request_jsoo_conv) request) in
    let url = Format.sprintf "%s?req=%s%s" base req id in
    let info = Windows.make_createData ~url ~typ:"popup" ~width ~height () in
    Lwt.map (fun _ -> ()) (Windows.create ?callback ~info ())
end
