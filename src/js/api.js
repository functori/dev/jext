const MAX = 4294967295
let id = Math.floor(Math.random() * MAX)
let cbs = {}
const api_src = "api"
const background_src = "background"
const client_src = "client"

window.addEventListener("message", function(ev) {
  if (ev.source == window && ev.data.id && cbs[ev.data.id] && (ev.data.src==background_src || ev.data.src==client_src)) {
    if (ev.data.ok) return cbs[ev.data.id]({ok: ev.data.output});
    else cbs[ev.data.id]({error: ev.data.output})
  }
}, false);

api = {
  request: function(input, cb) {
    id = (id + 1) % MAX
    let request = { id, input, src: api_src };
    if (cb != undefined) {
      cbs[id] = cb
      window.postMessage(request, "*")
    } else {
      let p = new Promise(function(resolve, reject) {
        window.addEventListener("message", function(ev) {
          try {
            if (ev.source == window && ev.data.id == id && (ev.data.src==background_src || ev.data.src==client_src)) {
              if (ev.data.ok) resolve(ev.data.output);
              else reject(ev.data.output)
            }
          } catch(error) { reject(error) }
        }, false);
        window.postMessage(request, "*")
      });
      return p
    }
  }
}
