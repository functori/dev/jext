open Chrome_lwt
open Common.Types

module Lib(S: S) = struct
  include Make(S)

  let send_req ?id ?(wait_callback=false) req_input =
    let info = Utils.Runtime.mk_connection_info "client" in
    let req_id = match id with
      | None -> Int32.to_int @@ Random.int32 Int32.max_int
      | Some i -> i in

    try
      let req = request_aux_to_jsoo S.request_jsoo_conv
          {req_id; req_input; req_src=`client} in
      let port = Runtime.connect ~info () in
      if wait_callback then
        let w, n = Lwt.wait () in
        Utils.Browser.addListener1 port##.onMessage (fun res ->
            try
              let res = response_aux_of_jsoo response_jsoo_conv res in
              match res.res_src with
              | `background when res.res_id = req_id ->
                port##disconnect;
                Lwt.wakeup n (Result.map Option.some res.res_output)
              | _ -> ()
            with exn ->
              Lwt.wakeup n (Error (`generic ("wrong response", Printexc.to_string exn))));
        port##postMessage req;
        w
      else (
        port##postMessage req;
        Lwt.return_ok None)
    with exn ->
      Lwt.return_error (`generic ("wrong request", Printexc.to_string exn))

  let () = Random.self_init ()
end
