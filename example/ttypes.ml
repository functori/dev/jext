type request =
  | A of int
  | B of string
  | G
[@@deriving jsoo]

type response_ok =
  | C of int
  | D of string
  | E of int
  | F of string
[@@deriving jsoo]

type response_error = string [@@deriving jsoo]
