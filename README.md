## jsoo_ext

jsoo_ext is a library to help make extension backgrounds and clients.

### build
```bash
make deps
make
```

### example
The sources in the example folder create a simple extension that will be located in _example after the build:
```bash
opam install vue-ppx
make dev
```
